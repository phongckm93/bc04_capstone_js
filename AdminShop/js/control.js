import { Phone } from "./model.js";

let phoneItem = (item, i) => {
  return `
<tr>
    <td>${i + 1}</td>
    <td>${item.name}</td>
    <td>${item.price}</td>
    <td><img style="width:80px;" src="${item.img}" alt="${item.name}"></td>
    <td><span>${item.frontCamera},${item.backCamera},${item.screen}</span><br/>
  <span>${item.desc}</span><br/>
  <span>${item.type}</span>
  </td>
    <td><button
     onclick="deletePhone('${
       item.id
     }')" class="btn btn-danger m-1"><i class="fa-solid fa-trash-can"></i></button><button onclick="updatePhone('${
    item.id
  }')" data-toggle="modal" data-target="#exampleModal" class="btn btn-success m-1"><i class="fa-solid fa-rotate"></i></button></td>
</tr>

`;
};
export let renderAll = (list) => {
  let nameSearch = document.getElementById("searchPhone").value;
  let contentHTML = "";
  if (nameSearch == "") {
    list.forEach((phone, index) => {
      let i = phoneItem(phone, index);
      contentHTML += i;
    });
  } else {
    let newList = [];
    list.forEach((item) => {
      if (item.type == nameSearch) {
        newList.push(item);
      }
    });
    newList.forEach((phone, index) => {
      let i = phoneItem(phone, index);
      contentHTML += i;
    });
  }

  document.getElementById("data").innerHTML = contentHTML;
};

export let getInfo = () => {
  let name = document.getElementById("phoneName").value;
  let price = document.getElementById("phonePrice").value;
  let frontCamera = document.getElementById("phoneCamera").value;
  let backCamera = document.getElementById("phoneBackCamera").value;
  let screen = document.getElementById("phoneScreen").value;
  let type = document.getElementById("phoneType").value;
  let img = document.getElementById("phoneLink").value;
  let desc = document.getElementById("phoneDesc").value;
  if (
    name == "" ||
    price == "" ||
    frontCamera == "" ||
    backCamera == "" ||
    screen == "" ||
    type == "" ||
    img == "" ||
    desc == ""
  ) {
    alert("Dữ liệu không được để trống, xin bạn kiểm tra!");
    return -1;
  }
  return new Phone(
    name,
    price,
    screen,
    backCamera,
    frontCamera,
    img,
    desc,
    type
  );
};
export let resetInput = () => {
  document.getElementById("phoneName").value = "";
  document.getElementById("phonePrice").value = "";
  document.getElementById("phoneCamera").value = "";
  document.getElementById("phoneBackCamera").value = "";
  document.getElementById("phoneScreen").value = "";
  document.getElementById("phoneType").value = "";
  document.getElementById("phoneLink").value = "";
  document.getElementById("phoneDesc").value = "";
};
export let onOff = () => {
  let change = document.getElementById("onOff");
  if (change.style.display == "none") {
    change.style.display = "block";
  } else {
    change.style.display = "none";
  }
};
