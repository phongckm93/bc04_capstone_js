import { getInfo, onOff, renderAll, resetInput } from "./control.js";
let idFind = 0;
let phoneList = [];
const BASE_URL = "https://62db6ca4d1d97b9e0c4f338f.mockapi.io";
let getData = () => {
  axios({
    url: `${BASE_URL}/phone`,
    method: "GET",
  })
    .then(function (res) {
      phoneList = [...res.data];
      renderAll(phoneList);
      onOff();
    })
    .catch(function (err) {
      alert("kiểm tra kêt nối mạng");
      console.log(err);
    });
};
getData();
let deletePhone = (id) => {
  axios({
    url: `${BASE_URL}/phone/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      console.log("delete", res.data);
      getData();
      onOff();
    })
    .catch(function (err) {
      alert("kiểm tra kêt nối mạng");
      console.log(err);
    });
};
window.deletePhone = deletePhone;
let updatePhone = (id) => {
  document.getElementById("upPhone").style.display = "block";
  document.getElementById("addPhone").style.display = "none";
  let index = phoneList.findIndex((item) => {
    return item.id == id;
  });
  idFind = id;
  document.getElementById("phoneName").value = phoneList[index].name;
  document.getElementById("phonePrice").value = phoneList[index].price;
  document.getElementById("phoneCamera").value = phoneList[index].frontCamera;
  document.getElementById("phoneBackCamera").value =
    phoneList[index].backCamera;
  document.getElementById("phoneScreen").value = phoneList[index].screen;
  document.getElementById("phoneType").value = phoneList[index].type;
  document.getElementById("phoneLink").value = phoneList[index].img;
  document.getElementById("phoneDesc").value = phoneList[index].desc;
};
window.updatePhone = updatePhone;

document.getElementById("upPhone").addEventListener("click", () => {
  let phoneUp = getInfo();
  if (phoneUp == -1) {
    return;
  }
  axios({
    url: `${BASE_URL}/phone/${idFind}`,
    method: "PUT",
    data: phoneUp,
  })
    .then(function (res) {
      console.log("res.data", res.data);
      getData();
      onOff();
      alert("Đã cập nhật sản phẩm");
    })
    .catch(function (err) {
      alert("kiểm tra kêt nối mạng");
      console.log(err);
    });
});

document.getElementById("addPhone").addEventListener("click", () => {
  let phoneAdd = getInfo();
  if (phoneAdd == -1) {
    return;
  }
  axios({
    url: `${BASE_URL}/phone`,
    method: "POST",
    data: phoneAdd,
  })
    .then(function (res) {
      console.log("phoneAdd", res);
      getData();
      onOff();
      alert("Đã thêm sản phẩm");
      resetInput();
    })
    .catch(function (err) {
      alert("kiểm tra kêt nối mạng");
      console.log(err);
    });
});
document.getElementById("modalButton").addEventListener("click", () => {
  document.getElementById("upPhone").style.display = "none";
  document.getElementById("addPhone").style.display = "block";
  resetInput();
});
document.getElementById("searchPhone").addEventListener("change",() => { 
  renderAll(phoneList);
 })
